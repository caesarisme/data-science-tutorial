from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression

clf = tree.DecisionTreeClassifier()
neigh = KNeighborsClassifier()
reg = LogisticRegression()

X = [[181, 80, 44], [177, 70, 43], [160, 60, 38], [154, 54, 37], [166, 65, 40],
     [190, 90, 47], [175, 64, 39],
     [177, 70, 40], [159, 55, 37], [171, 75, 42], [181, 85, 43]]


Y = ['male', 'male', 'female', 'female', 'male', 'male', 'female', 'female',
     'female', 'male', 'male']

clf = clf.fit(X, Y)
neigh = neigh.fit(X, Y)
reg = reg.fit(X, Y)

tree_prediction = clf.predict([[190, 64, 43]])
neigh_prediction = neigh.predict([[190, 64, 43]])
reg_prediction = reg.predict([[190, 64, 44]])

print('tree', tree_prediction)
print('neigh', neigh_prediction)
print('reg', reg_prediction)